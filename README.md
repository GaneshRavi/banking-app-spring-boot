Building a executable/deployable war file
------------------------------------------
To build the war, execute the following command from the root of the project directory:

	$ mvn package


Executing the application war file
----------------------------------
To execute the application, simply execute the war file:

    $ java -jar target/banking-app.war


Deploying the application war file to a Web Server
--------------------------------------------------
To deploy the application war file to a Web Server, like Tomcat (the web server should support Servlet 3.0 or above), simply copy the 'target/banking-app.war' to the 'webapps' directory of the Web Server.


The REST url:
--------------
The REST url of the application is the following:

    http://localhost:8080/banking-app/

The various REST endpoints are

    http://localhost:8080/banking-app/users
	http://localhost:8080/banking-app/user/{userid}
	http://localhost:8080/banking-app/user/{userid}/accounts
	http://localhost:8080/banking-app/user/{userid}/recipients
	http://localhost:8080/banking-app/currencies


Spring Boot Actuator Port for monitoring:
-----------------------------------------
The port configured to be used by Spring boot actuator is 8081. There are various end-points available for monitoring like health, metrics, env, trace, mappings, shutdown, etc. An example usage is shown below:

    $ curl localhost:8081/health
    {"status":"UP"}