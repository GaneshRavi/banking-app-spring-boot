package com.banking.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.banking.exception.AccountsNotFoundException;
import com.banking.exception.RecipientsNotFoundException;
import com.banking.exception.UserNotFoundException;
import com.banking.model.UserModel;
import com.banking.resource.CurrenciesUtil;
import com.banking.resource.Currency;
import com.banking.resource.Recipient;
import com.banking.resource.User;

@RestController
public class MainController {

	@RequestMapping(value = { "/user/{userid}/recipients" }, method = RequestMethod.GET)
	public List<Recipient> getRecipientsForUser(
			@PathVariable("userid") long userid) throws UserNotFoundException,
			RecipientsNotFoundException {
		User user = UserModel.getUser(userid);
		if (user == null) {
			throw new UserNotFoundException(userid);
		}
		List<Recipient> recipients = UserModel.getUser(userid).getRecipients();
		if (!recipients.isEmpty()) {
			return recipients;
		} else {
			throw new RecipientsNotFoundException(user);
		}
	}

	@RequestMapping(value = { "/user/{userid}/accounts" }, method = RequestMethod.GET)
	public List<Long> getAccountsForUser(@PathVariable("userid") long userid)
			throws AccountsNotFoundException, UserNotFoundException {
		User user = UserModel.getUser(userid);
		if (user == null) {
			throw new UserNotFoundException(userid);
		}
		List<Long> accountIds = UserModel.getUser(userid).getAccountIds();
		if (!accountIds.isEmpty()) {
			return accountIds;
		} else {
			throw new AccountsNotFoundException(user);
		}
	}

	@RequestMapping(value = { "/user/{userid}" }, method = RequestMethod.GET)
	public User getUserDetails(@PathVariable("userid") long userid)
			throws UserNotFoundException {
		User user = UserModel.getUser(userid);
		if (user == null) {
			throw new UserNotFoundException(userid);
		} else {
			return user;
		}
	}
	
	@RequestMapping(value = { "/users" }, method = RequestMethod.GET)
	public List<User> getUsers() {
		return UserModel.getUsers();
	}

	@RequestMapping(value = { "/currencies" }, method = RequestMethod.GET)
	public @ResponseBody List<Currency> getCurrencies() {
		return CurrenciesUtil.getCurrencies();
	}
	

}