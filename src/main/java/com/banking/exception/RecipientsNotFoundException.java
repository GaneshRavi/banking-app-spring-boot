package com.banking.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.banking.resource.User;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Recipients Not Found")
public class RecipientsNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public RecipientsNotFoundException(User user){
		super("Accounts Not Found for user = " + user.getId());
	}
}