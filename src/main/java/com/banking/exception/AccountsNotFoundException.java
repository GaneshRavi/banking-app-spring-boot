package com.banking.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.banking.resource.User;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Accounts Not Found")
public class AccountsNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public AccountsNotFoundException(User user){
		super("Accounts Not Found for user = " + user.getId());
	}
}