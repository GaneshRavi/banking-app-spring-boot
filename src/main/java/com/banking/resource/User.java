package com.banking.resource;

import java.util.ArrayList;
import java.util.List;

public class User {

    private final long id;
    private final String firstName;
	private final String lastName;
	private List<Long> accountIds = new ArrayList<>();
    
	private List<Recipient> recipients = new ArrayList<>();

    public User(long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public long getId() {
        return id;
    }
    
    public List<Long> getAccountIds() {
		return accountIds;
	}
    
    public void addAccount(Long accountId) {
    	accountIds.add(accountId);
    }
    
    public void deleteAccount(Long accountId) {
    	accountIds.remove(accountId);
    }
    
    public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

    public void addRecipient(Recipient recipient) {
        recipients.add(recipient);
    }
    
    public void deleteRecipient(Recipient recipient) {
        recipients.remove(recipient);
    }
    
    public List<Recipient> getRecipients() {
    	return recipients;
    }
}