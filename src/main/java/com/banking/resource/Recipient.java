package com.banking.resource;

public class Recipient {

	private final long accountNumber;
	
	private final String firstName;
	private final String lastName;
	
	public Recipient(long accountNumber, String firstName, String lastName) {
		this.accountNumber = accountNumber;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
}
