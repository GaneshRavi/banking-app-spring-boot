package com.banking.resource;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CurrenciesUtil {
	
	private static List<Currency> currencies;

	public static List<Currency> getCurrencies() {
		if(currencies == null) {
			readJson();
		}
		return currencies;
	}
	
	private static void readJson() {
		JSONParser parser = new JSONParser();
		currencies = new ArrayList<>();
		try {

			Object obj = parser.parse(new FileReader("src/main/resources/currencies.json"));

			JSONObject jsonObject = (JSONObject) obj;

			Iterator<?> keysIterator = jsonObject.keySet().iterator();
			while(keysIterator.hasNext()) {
				JSONObject item = (JSONObject) jsonObject.get(keysIterator.next());
				currencies.add(new Currency((String)item.get("name"), (String)item.get("symbol")));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
}
