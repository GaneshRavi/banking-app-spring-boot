package com.banking.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.banking.resource.Recipient;
import com.banking.resource.User;

public class UserModel {

	private static Map<Long, User> users = new HashMap<>();
	
	static {
		User user1 = (new User(1L, "John1", "Doe1"));
		user1.addAccount(12345L);
		user1.addAccount(12346L);
		user1.addAccount(12347L);
		user1.addRecipient(new Recipient(22222L, "Mary1", "Jane1"));
		user1.addRecipient(new Recipient(33333L, "Mary2", "Jane2"));
		addUser(user1);
		
		User user2 = (new User(2L, "John2", "Doe2"));
		user2.addAccount(67890L);
		user2.addAccount(67891L);
		user2.addRecipient(new Recipient(44444L, "Peter1", "Parker1"));
		user2.addRecipient(new Recipient(55555L, "Peter2", "Parker2"));
		addUser(user2);
	}
	
	private UserModel() {
	}
	
	public static User getUser(long userid) {
		return users.get(userid);
	}
	
	/**
	 * @param userid The USER ID to be added
	 * @param user The User to be added
	 * 
	 * @return The User that was added
	 */
	public static User addUser(User user) {
		users.put(user.getId(), user);
		return users.get(user.getId());
	}
	
	/**
	 * @param userid The User ID to be deleted
	 * 
	 * @return The User that was deleted
	 */
	public static User deleteUser(long userid) {
		return users.remove(userid);
	}
	
	public static List<User> getUsers() {
		return  new ArrayList<>(users.values());
	}
	
}